import cv2
import numpy as np
from matplotlib import pyplot as plt


directory = 'worldscreen/games/bubble_beat'


dimensions = [
    # '1024x768',
    # '1136x640',
    # '2048x1536',
    # '2208x1242',
    # '2436x1125',
    # '2560x1440',
    '2960x1440'
]
screens = [
    # "create_account",
    # 'info_slider',
    # 'plan_chooser',
    # 'age_gate',
    # 'shows_title',
    # 'games_title',
    # 'books_title',
    # 'create_title',
    # 'grow_title',
    # 'music_title',
    # 'shows_turtorial',
    # 'shows_select',
    # 'start_grow_game',
    # 'rain_umbrella',
    # 'sun_glasses',
    # 'end_grow_game',
    # 'grownup_popup'
    # 'login_popup'
    # 'logged_in_hooray'
    # 'email_view',
    # 'email_view_2'
    # "grownups",
    # 'create_account',
    # 'create_account_next',
    # 'gdpr_error_msg',
    # "empty_email_popup_error_msg"
    # 'settings_tab',
    # 'settings_cog',
    # 'email_scene',
    # 'fb_login_btn'
    # 'mistake_passwd_error_msg',
    # 'mistake_email_error_msg',
    # 'empty_email_error_msg',
    # 'empty_passwd_error_msg',
    # 'gdpr_error_msg',
    # 'offline_error_msg',
    # 'existing_email_error_msg'
    # 'restore_subscription_error_msg',
    # 'restore_subscription_btn',
    # 'compare_login_popup_screenshot'
    # 'games_select',
    # 'games_tutorial'
    # 'play_btn',
    # 'title_sense'
    # 'game_play',
    'learn_game_mi',
    'learn_game_so',
    # 'game_play_pause'
    # 'game_title'
    # 'game_download',
    # 'game_download_2',
    # 'bubble_beat',
    # 'share',
    # 'sense',
    # 'find',
    # 'numbrrrs'
    # "sheep_count",
    # "frog_sums"
    # "monster_match"
    # "padlock"
    # 'rockflip',
    # 'abc_hotel_download'
    # 'gm_play_btn',
    # 'gm_back_btn'
    # 'abc_hotel'
    # 'title_bubble_beat',
    # 'learn_btn'

]

login = {
    '1': 8,
    '2': 9,
    '3': 10,
    '4': 11,
    '.': 56,
    '@': 77,
    'E': 33,
    'H': 36,
    'O': 43,
    'P': 44,
    'R': 46,
    'S': 47,
    'T': 48
}

for dimension in dimensions:
    for screen in screens:

        actual_template = f'./images/{directory}/{dimension}_{screen}_actual.png'
        img = cv2.imread(actual_template, 0)
        # img =r cv2.resize(img, (1024, 768), interpolation=cv2.INTER_CUBIC)

        img2 = img.copy()
        template_template = f'./images/{directory}/{dimension}_{screen}_template.png'
        template = cv2.imread(template_template, 0)

        w, h = template.shape[::-1]

        coef_x = 1024 / 2048
        coef_y = 768 / 1536

        # All the 6 methods for comparison in a list
        methods = [
            'cv2.TM_CCOEFF',
            'cv2.TM_CCOEFF_NORMED',
            'cv2.TM_CCORR',
            'cv2.TM_CCORR_NORMED',
            'cv2.TM_SQDIFF',
            'cv2.TM_SQDIFF_NORMED'
        ]

        match_dict = {}
        print(dimension)
        print(screen)
        for meth in methods:
            img = img2.copy()
            method = eval(meth)

            # Apply template Matching
            res = cv2.matchTemplate(img, template, method)
            # y, x = (np.unravel_index(res.argmax(), res.shape))

            min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

            # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
            if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
                top_left = min_loc

            else:
                top_left = max_loc
            bottom_right = (top_left[0] + w, top_left[1] + h)

            cv2.rectangle(img, top_left, bottom_right, 255, 2)
            x, y = top_left
            x = top_left[0] + (bottom_right[0] - top_left[0])/2
            y = top_left[1] + (bottom_right[1] - top_left[1])/2
            cv2.circle(img, (int(x), int(y)), 20, (0, 255, 0), cv2.LINE_4)

            plt.subplot(121), plt.imshow(res, cmap='gray')
            plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
            plt.subplot(122), plt.imshow(img, cmap='gray')
            plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
            plt.suptitle(meth)

            plt.show()
            key = (x, y)
            if key in match_dict:
                match_dict[key] += 1
            else:
                match_dict.update({key: 1})
            print(f'{x * coef_x} : {y * coef_y} {meth}')
        print(match_dict)
        print('\n')
