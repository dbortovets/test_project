import numpy as np
import cv2

im = cv2.imread("./images/white_rect.jpg")
# im = np.zeros((512, 512, 3), np.uint8)
imgray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(imgray, 127, 255, 0)
image, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)


# To draw all the contours in an image:
img = cv2.drawContours(image, contours, -1, (0, 500, 500), 3)


# To draw an individual contour, say 4th contour:
# cnt = contours[4]
# img = cv2.drawContours(image, [cnt], 0, (0, 255, 0), 3)

# cv2.namedWindow('dbr', cv2.WINDOW_NORMAL)
cv2.imshow('dbr', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
print("hello")