import cv2
import numpy as np,sys

A = cv2.imread('./images/the_doberman.jpg')
# B = cv2.imread('orange.jpg')

# generate Gaussian pyramid for A
G = A.copy()
gpA = [G]
for i in range(6):
    G = cv2.pyrDown(G)
    gpA.append(G)

    cv2.namedWindow('dober', cv2.WINDOW_NORMAL)
    cv2.imshow('dober', G)
    cv2.waitKey(0)
    cv2.destroyAllWindows()