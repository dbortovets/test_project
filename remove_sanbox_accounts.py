import re
from time import sleep

import argparse

from selenium.common.exceptions import ElementClickInterceptedException
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


parser = argparse.ArgumentParser()
parser.add_argument('--it', type=int)
parser.add_argument('--pattern', type=str, default='^([0-9]+@)')
args = parser.parse_args()


class AppStoreConnect:

    url = "https://appstoreconnect.apple.com/"
    spinner_create_user = (By.XPATH, "//div[@class=' tb-loading__spinner']")
    page_spinner = (By.ID, "pageSpinner")
    download_spin = (By.XPATH, "//div[@class='spin-el']")
    spinner = (By.XPATH, "//div[@class='spinner']")
    loging_iframe = (By.ID, 'aid-auth-widget-iFrame')
    apple_id_text_field = (By.ID, "account_name_text_field")
    passwd_text_field = (By.ID, "password_text_field")
    sign_in_btn = (By.ID, "sign-in")

    user_and_access_div = (By.XPATH, "//div[contains(text(), 'Users and Access')]")
    testers_btn = (By.XPATH, "//a[contains(text(), 'Testers')]")

    main_delete_btn = (By.XPATH, "//button[contains(text(), 'Delete')]")
    popup_delete_btn = (By.XPATH, "//div[@class='tb-btn-group']/button[contains(text(), 'Delete')]")
    edit_btn = (By.XPATH, "//button[contains(text(), 'Edit')]")
    user_container = "//div[@class='ReactVirtualized__Grid__innerScrollContainer']"
    sand_box_checkboxes = (By.XPATH, f"{user_container}/div/div/input")
    sanbox_ids = (By.XPATH, f"{user_container}/div/div[2]")

    def __init__(self):
        option = Options()
        option.add_argument("--headless")
        self.driver = Firefox()#options=option)
        self.driver.implicitly_wait(20)
        self.driver.maximize_window()

    def enter_text_into_input(self, text, web_element, timeout=60):
        element = getattr(self, web_element)
        WebDriverWait(driver=self.driver, timeout=timeout).until(ec.visibility_of_element_located(element))
        text_field = self.driver.find_element(*element)
        text_field.clear()
        text_field.send_keys(text)

    def click_sign_in_btn(self, timeout=60):
        WebDriverWait(driver=self.driver, timeout=timeout) \
            .until(ec.visibility_of_element_located(self.sign_in_btn))
        sign_in_btn = self.driver.find_element(*self.sign_in_btn)
        sign_in_btn.click()

    def click_users_and_access(self, timeout=60):
        sleep(5)
        WebDriverWait(driver=self.driver, timeout=timeout)\
            .until(ec.invisibility_of_element_located(self.spinner_create_user))
        WebDriverWait(driver=self.driver, timeout=timeout) \
            .until(ec.invisibility_of_element_located(self.page_spinner))
        WebDriverWait(driver=self.driver, timeout=timeout) \
            .until(ec.visibility_of_element_located((By.ID, "main-nav")))
        WebDriverWait(driver=self.driver, timeout=timeout)\
            .until(ec.element_to_be_clickable(self.user_and_access_div))
        users_and_access = self.driver.find_element(*self.user_and_access_div)
        users_and_access.click()

    def click_testers_btn(self, timeout=60):
        WebDriverWait(driver=self.driver, timeout=timeout) \
            .until(ec.invisibility_of_element(self.download_spin))
        testers_btn = self.driver.find_element(*self.testers_btn)
        testers_btn.click()

    def click_edit_btn(self, timeout=60):
        WebDriverWait(driver=self.driver, timeout=timeout) \
            .until(ec.invisibility_of_element(self.download_spin))
        testers_btn = self.driver.find_element(*self.edit_btn)
        testers_btn.click()

    def click_delete_btn(self, main_delete=True):
        delete_btn = self.main_delete_btn if main_delete else self.popup_delete_btn
        testers_btn = self.driver.find_element(*delete_btn)
        testers_btn.click()

    def delete_group_of_sandbox_accounts(self, re_pattern='^([0-9]+@)'):

        check_boxes = self.driver.find_elements(*self.sand_box_checkboxes)
        sanbox_ids = self.driver.find_elements(*self.sanbox_ids)
        delete_elemet = 0
        for i, sand_box in enumerate(sanbox_ids):
            sand_box_text = sand_box.text
            if re_pattern:
                if re.match(re_pattern, sand_box_text):
                    try:
                        check_boxes[i].click()
                        delete_elemet += 1
                    except ElementClickInterceptedException as exc:
                        print(exc.args)
                        check_boxes[i].location_once_scrolled_into_view()
                        check_boxes[i].click()
            else:
                check_boxes[i].click()
        return delete_elemet

    def go_to_sandbox_accounts(self):
        self.driver.get(self.url)

        WebDriverWait(driver=self.driver, timeout=60).until(ec.visibility_of_element_located(self.loging_iframe))
        self.driver.switch_to.frame(self.driver.find_element(*self.loging_iframe))
        self.enter_text_into_input(text='automation.tests@platomedia.tv', web_element='apple_id_text_field')
        self.click_sign_in_btn()
        self.enter_text_into_input(text='HopsterTests#1', web_element='passwd_text_field')
        self.click_sign_in_btn()
        self.driver.switch_to.default_content()
        self.click_users_and_access()
        self.click_testers_btn()

    def delete_all_automation_sanbox_accoutns(self, iteration=5, pattern='^([0-9]+@)'):

        self.go_to_sandbox_accounts()
        for i in range(iteration):
            self.click_edit_btn()
            # self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            self.driver.execute_script("scroll(0, 0);")
            sleep(1)
            delete_elements = self.delete_group_of_sandbox_accounts(re_pattern=pattern)
            if delete_elements == 0:
                break
            self.click_delete_btn()
            sleep(1)
            self.click_delete_btn(main_delete=False)


if __name__ == '__main__':

    iteration = args.it
    pattern = args.pattern
    sandbox = AppStoreConnect()

    try:
        sandbox.delete_all_automation_sanbox_accoutns(iteration=iteration, pattern=pattern)
    finally:
        sandbox.driver.close()
