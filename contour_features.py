import cv2
import numpy as np

img = cv2.imread("./images/the_doberman.jpg", 0)
# im_bw = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
ret, thresh = cv2.threshold(img, 127, 255, 0)
image, contours, hierarchy = cv2.findContours(thresh, 1, 2)

cnt = contours[0]
M = cv2.moments(cnt)
print(M)
cx = int(M['m10']/M['m00'])
cy = int(M['m01']/M['m00'])

print("Centroid\ncx = %s\ncy = %s" % (cx, cy))

area = cv2.contourArea(cnt)
print('Contour Area\n %s' % area)


perimeter = cv2.arcLength(cnt, True)
print('Perimeter = %s' % perimeter)