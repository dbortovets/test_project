import numpy as np
import cv2
from matplotlib import pyplot as plt

img = cv2.imread("./images/IMAG0911.jpg")
# Basic how to read, show and write image

cv2.namedWindow('dober', cv2.WINDOW_NORMAL)
cv2.imshow('dober', img)
cv2.waitKey(0)
cv2.destroyAllWindows()

cv2.imwrite("./images/dober.png", img)


# # Using matplotlib
#
# plt.imshow(img, cmap="gray", interpolation="bicubic")
# plt.xticks([]), plt.yticks([])
# plt.show()
