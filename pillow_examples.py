from PIL import Image, ImageChops

# name = "800X600_"
# for s in ['shows', 'games', 'books', 'downloads', 'create', 'grow', 'music']:
#     img = Image.open(f'./images/templates/{s}_actual.png')
#     img = img.resize((800, 600), Image.ANTIALIAS)
#     new = img.save(f'./images/templates/{name}_{s}_actual.png', dpi=(72, 72))


def equal(im1, im2):
    """Returns True if two images are equal"""
    img1 = Image.open(im1)
    img2 = Image.open(im2)
    return ImageChops.difference(img1, img2).getbbox() is None


a = equal(im1='/Users/dbr13/PycharmProjects/Test_project/images/age_gate/1024x768_age_gate_template.png',
      im2='/Users/dbr13/PycharmProjects/Test_project/images/age_gate/1024x768_age_gate_template.png')
print(a)